import createMiddleware from 'next-intl/middleware';
 
export default createMiddleware({
  // All locales across all domains
  locales: ['en', 'ar'],
 
  // Used when no domain matches (e.g. on localhost)
  defaultLocale: 'ar',
 

});

export const config = {
  // Match only internationalized pathnames
  matcher: ['/', '/(ar|en)/:path*']
};